import ClassicEditorBase from "@ckeditor/ckeditor5-editor-classic/src/classiceditor";
import EssentialsPlugin from "@ckeditor/ckeditor5-essentials/src/essentials";
import UploadAdapterPlugin from "@ckeditor/ckeditor5-adapter-ckfinder/src/uploadadapter";
import AutoformatPlugin from "@ckeditor/ckeditor5-autoformat/src/autoformat";
import BoldPlugin from "@ckeditor/ckeditor5-basic-styles/src/bold";
import ItalicPlugin from "@ckeditor/ckeditor5-basic-styles/src/italic";
import UnderlinePlugin from "@ckeditor/ckeditor5-basic-styles/src/underline";
import BlockQuotePlugin from "@ckeditor/ckeditor5-block-quote/src/blockquote";
import EasyImagePlugin from "@ckeditor/ckeditor5-easy-image/src/easyimage";
import HeadingPlugin from "@ckeditor/ckeditor5-heading/src/heading";
import ImagePlugin from "@ckeditor/ckeditor5-image/src/image";
import ImageCaptionPlugin from "@ckeditor/ckeditor5-image/src/imagecaption";
import ImageStylePlugin from "@ckeditor/ckeditor5-image/src/imagestyle";
import ImageToolbarPlugin from "@ckeditor/ckeditor5-image/src/imagetoolbar";
import ImageInsert from "@ckeditor/ckeditor5-image/src/imageinsert";
import ImageUploadPlugin from "@ckeditor/ckeditor5-image/src/imageupload";
import ImageResize from "@ckeditor/ckeditor5-image/src/imageresize";
import ImageResizeEditing from "@ckeditor/ckeditor5-image/src/imageresize/imageresizeediting";
import ImageResizeHandles from "@ckeditor/ckeditor5-image/src/imageresize/imageresizehandles";
import ImageResizeButtons from "@ckeditor/ckeditor5-image/src/imageresize/imageresizebuttons";
import LinkPlugin from "@ckeditor/ckeditor5-link/src/link";
import LinkImage from "@ckeditor/ckeditor5-link/src/linkimage";
import ListPlugin from "@ckeditor/ckeditor5-list/src/list";
import ParagraphPlugin from "@ckeditor/ckeditor5-paragraph/src/paragraph";
import Table from "@ckeditor/ckeditor5-table/src/table";
import TableToolbar from "@ckeditor/ckeditor5-table/src/tabletoolbar";
import TableProperties from "@ckeditor/ckeditor5-table/src/tableproperties";
import TableCellProperties from "@ckeditor/ckeditor5-table/src/tablecellproperties";
import Indent from "@ckeditor/ckeditor5-indent/src/indent";
import IndentBlock from "@ckeditor/ckeditor5-indent/src/indentblock";
import MediaEmbed from "@ckeditor/ckeditor5-media-embed/src/mediaembed";

export default class ClassicEditor extends ClassicEditorBase {}

ClassicEditor.builtinPlugins = [
  EssentialsPlugin,
  UploadAdapterPlugin,
  AutoformatPlugin,
  BoldPlugin,
  ItalicPlugin,
  UnderlinePlugin,
  BlockQuotePlugin,
  EasyImagePlugin,
  HeadingPlugin,
  ImagePlugin,
  ImageCaptionPlugin,
  ImageStylePlugin,
  ImageToolbarPlugin,
  ImageUploadPlugin,
  ImageInsert,
  ImageResize,
  ImageResizeEditing,
  ImageResizeHandles,
  ImageResizeButtons,
  LinkPlugin,
  LinkImage,
  ListPlugin,
  ParagraphPlugin,
  Table,
  TableToolbar,
  TableProperties,
  TableCellProperties,
  Indent,
  IndentBlock,
  MediaEmbed,
];

ClassicEditor.defaultConfig = {
  toolbar: {
    items: [
      "heading",
      "|",
      "bold",
      "italic",
      "underline",
      "|",
      "bulletedList",
      "numberedList",
      "blockQuote",
      "|",
      "outdent",
      "indent",
      "|",
      "link",
      "imageUpload",
      "insertTable",
      "mediaEmbed",
      "|",
      "undo",
      "redo",
    ],
  },
  image: {
    styles: ["alignLeft", "alignCenter", "alignRight", "full", "side"],
    resizeUnit: "%",
    resizeOptions: [
      {
        name: "resizeImage:original",
        value: null,
        icon: "original",
      },
      {
        name: "resizeImage:25",
        value: "25",
        icon: "small",
      },
      {
        name: "resizeImage:50",
        value: "50",
        icon: "medium",
      },
      {
        name: "resizeImage:75",
        value: "75",
        icon: "large",
      },
    ],

    toolbar: [
      "imageStyle:full",
      "imageStyle:side",
      "|",
      "imageStyle:alignLeft",
      "imageStyle:alignCenter",
      "imageStyle:alignRight",
      "|",
      "resizeImage:25",
      "resizeImage:50",
      "resizeImage:75",
      "resizeImage:original",
      "|",
      "imageTextAlternative",
      "linkImage",
    ],
  },
  table: {
    contentToolbar: [
      "tableColumn",
      "tableRow",
      "mergeTableCells",
      "tableProperties",
      "tableCellProperties",
    ],
  },
  indentBlock: {
    offset: 1,
    unit: "em",
  },
  language: "en",
  // cloudServices: {
  //   tokenUrl: () =>
  //     new Promise((resolve, reject) => {
  //       const xhr = new XMLHttpRequest();
  //       const params = {
  //         email: "ly.khuong1996@gmail.com",
  //         password: "0919773165",
  //       };
  //       xhr.open("POST", "https://v3n.muvi.vn/oauth/auth/login");
  //       xhr.addEventListener("load", () => {
  //         const statusCode = xhr.status;
  //         const xhrResponse = xhr.response;

  //         if (statusCode < 200 || statusCode > 299) {
  //           return reject(new Error("Cannot download new token!"));
  //         }
  //         return resolve("Bearer " + JSON.parse(xhrResponse).access_token);
  //       });

  //       xhr.addEventListener("error", () => reject(new Error("Network Error")));
  //       xhr.addEventListener("abort", () => reject(new Error("Abort")));

  //       xhr.setRequestHeader("Content-type", "application/json");
  //       xhr.setRequestHeader(
  //         "authorization",
  //         "Basic c3RhZ2luZy5tdXZpLndlYjpDZFNEUmNyM0d3"
  //       );

  //       xhr.send(JSON.stringify(params));
  //     }),
  //   uploadUrl: "https://v3n.muvi.vn/api/client/upload?style=-1",
  // },
};
